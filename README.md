# Portail des associations

## Development :
### Backend :
The backend uses php with the Laravel framework.
You will need to install [php](https://www.php.net/) and [Composer](https://getcomposer.org/).

To install the required packages, run in the project folder root :
```
composer update
composer install
```

Then, create a copy of the `.env.example` file, named `.env` and run this command to the project root to generate an application key for the local app :
```
php artisan key:generate
```

To start the dev server :
```
php artisan serve
```

An error about the database might show up. To fix that, follow the next section.

### Database :
This web app also requires a database to work.

If you already have a database server running, you can configure database credentials in the `.env` file of the project folder root.

Otherwise, you can setup a local database using [MariaDB](https://mariadb.com/). MariaDB is also included in tools like [MAMP](https://www.mamp.info/en/downloads/) which makes the installation easier.

After you got your local database running, modify the database credentials in the `.env` file accordingly.
Then, to create the initial database, run in the project folder root :
```
php artisan migrate
```

### Frontend :
The frontend is powered by [React](https://react.dev/) and [Inertia](https://inertiajs.com/) connects it to the Laravel backend. [Vite](https://vite.dev/) is used for the frontend dev server because of its hot reload and fast compilation capabilities.

In order to install the packages, you will need `npm` (Node Package Manager) included in [Node.js](https://nodejs.org/en/download/).

To install the required packages, run in the project root :
```
npm install
```

To start the frontend server :
```
npm run dev
```

### Resources
[React documentation](https://react.dev/learn/start-a-new-react-project)

[React/Laravel Sample App](https://github.com/bigcommerce/laravel-react-sample-app/tree/master)

[Here is a small tutorial to watch](https://www.freecodecamp.org/news/use-react-with-laravel/)
