import React, { useState } from 'react';
import Header from '../Components/Header';
import MessageBar from '../Components/MessageBar';
import { Flowbite } from "flowbite-react";
import PDATheme from '../PDATheme';

const Test = () => {
    return (
        <Flowbite theme={{ theme: PDATheme }}>
            <Header firstname="Prénom" lastname="Nom" email="prenom.nom@etu.utc.fr"/>
            <MessageBar/>
            <h1>Hello World !</h1>
        </Flowbite>
    )
}

export default Test
