export default function MessageBar() {
    return(
        <div class="slide w-full text-center p-1 relative -z-10 bg-StrokeInLightMode dark:bg-Gray-7 text-sm text-gray-900 dark:text-OffWhite">
            {/* TODO : Message as component prop ? */}
            <span class="inline-block relative -z-10">Bienvenue ! Tu utilises actuellement la nouvelle version du Portail des Assos, fraîchement développée par le SiMDE. N'hésites pas à nous contacter si tu découvres un bug.</span>
        </div>
    );
}
