import HeaderButton from '../Components/HeaderButton';
import { Dropdown } from "flowbite-react";
import { ToggleSwitch } from "flowbite-react";
import { useState } from "react";
import { useThemeMode } from "flowbite-react";
import { Book, Person, Search, Power, BrightnessHigh, Moon } from "react-bootstrap-icons";
import { Avatar } from "flowbite-react";

export default function Header({firstname = '', lastname = '', email = ''}) {
    const [languageSwitch, language] = useState(false); /*TODO : Link to persistent value */
    const { mode, toggleMode } = useThemeMode(); /* Using useThemeMode() hook to get and toggle the mode */
	
	const [isExpanded, expand] = useState(false);
	const expandNavbar = () => {
		expand(!isExpanded);
	};

    return (
		<header class="sticky top-0 left-0 shadow">
			<nav class="bg-Yellow-1 text-black">
				<div class="flex flex-wrap items-center justify-between select-none w-full p-4">
					{/* Logo and Title */}
					<a class="flex content-center items-center cursor-pointer">
						<div class="bg-Gray-7 rounded-full">
							<img class="h-[50px] w-[50px] p-[5px]" src="logo.svg"/>
						</div>
						<h1 class="text-3xl font-Logo px-[10px] max-sm:hidden whitespace-nowrap">Portail des Assos</h1>
					</a>

					{/* Profile and mobile expand button*/}
					<div class="flex items-center space-x-3 ml-auto lg:order-2">
						<Dropdown dismissOnClick={false} renderTrigger={() =>
							<div class="cursor-pointer">
								<Avatar placeholderInitials={`${firstname[0]}${lastname[0]}`} sizing="sm" rounded />
							</div>}>

							<Dropdown.Header className="select-text">
								<span className="block text-sm">{firstname} {lastname}</span>
								<span className="block truncate text-sm font-medium">{email}</span>
							</Dropdown.Header>

							<Dropdown.Item>
							<div class="flex items-center gap-3">
								{/*SVG icons from svgrepo.com, Twemoji Collection (Twitter)*/}
								<svg width="20px" height="20px" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--twemoji" preserveAspectRatio="xMidYMid meet" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path fill="#ED2939" d="M36 27a4 4 0 0 1-4 4h-8V5h8a4 4 0 0 1 4 4v18z"></path><path fill="#002495" d="M4 5a4 4 0 0 0-4 4v18a4 4 0 0 0 4 4h8V5H4z"></path><path fill="#EEE" d="M12 5h12v26H12z"></path></g></svg>
								<ToggleSwitch checked={languageSwitch} sizing="sm" onChange={language} color="primary"/>
								<svg width="20px" height="20px" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--twemoji" preserveAspectRatio="xMidYMid meet" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path fill="#00247D" d="M0 9.059V13h5.628zM4.664 31H13v-5.837zM23 25.164V31h8.335zM0 23v3.941L5.63 23zM31.337 5H23v5.837zM36 26.942V23h-5.631zM36 13V9.059L30.371 13zM13 5H4.664L13 10.837z"></path><path fill="#CF1B2B" d="M25.14 23l9.712 6.801a3.977 3.977 0 0 0 .99-1.749L28.627 23H25.14zM13 23h-2.141l-9.711 6.8c.521.53 1.189.909 1.938 1.085L13 23.943V23zm10-10h2.141l9.711-6.8a3.988 3.988 0 0 0-1.937-1.085L23 12.057V13zm-12.141 0L1.148 6.2a3.994 3.994 0 0 0-.991 1.749L7.372 13h3.487z"></path><path fill="#EEE" d="M36 21H21v10h2v-5.836L31.335 31H32a3.99 3.99 0 0 0 2.852-1.199L25.14 23h3.487l7.215 5.052c.093-.337.158-.686.158-1.052v-.058L30.369 23H36v-2zM0 21v2h5.63L0 26.941V27c0 1.091.439 2.078 1.148 2.8l9.711-6.8H13v.943l-9.914 6.941c.294.07.598.116.914.116h.664L13 25.163V31h2V21H0zM36 9a3.983 3.983 0 0 0-1.148-2.8L25.141 13H23v-.943l9.915-6.942A4.001 4.001 0 0 0 32 5h-.663L23 10.837V5h-2v10h15v-2h-5.629L36 9.059V9zM13 5v5.837L4.664 5H4a3.985 3.985 0 0 0-2.852 1.2l9.711 6.8H7.372L.157 7.949A3.968 3.968 0 0 0 0 9v.059L5.628 13H0v2h15V5h-2z"></path><path fill="#CF1B2B" d="M21 15V5h-6v10H0v6h15v10h6V21h15v-6z"></path></g></svg>
							</div>
							</Dropdown.Item>
							<Dropdown.Item onClick={toggleMode} icon={mode === "dark" ? BrightnessHigh : Moon}>Changer de thème</Dropdown.Item>
							<Dropdown.Divider/>
							<Dropdown.Item icon={Person}>Mon compte</Dropdown.Item>
							<Dropdown.Item icon={Power}>Déconnexion</Dropdown.Item>
						</Dropdown>
						
						{/* Expand Mobile menu toggle */}
						<button class="inline-flex lg:hidden items-center justify-center p-2 w-8 h-8 hover:bg-OffWhite dark:hover:bg-Gray-7 rounded-lg dark:hover:text-OffWhite focus:outline-none focus:ring-2 focus:ring-Gray-3 dark:focus:ring-Gray-9" onClick={expandNavbar} aria-controls="navbar-buttons" aria-expanded={isExpanded}>
							<svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
								<path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"></path>
							</svg>
						</button>
					</div>

					{/* Buttons */}
					<div id="navbar-buttons" aria-hidden={!isExpanded} class={`flex flex-col lg:flex-row items-center gap-1 lg:gap-3 grow w-full lg:flex lg:justify-center lg:w-auto lg:order-1 bg-OffWhite dark:bg-Gray-5 lg:bg-transparent lg:dark:bg-transparent p-4 lg:p-0 mt-4 lg:mt-0 border border-Gray-1 dark:border-Gray-7 rounded-lg lg:border-0 ${isExpanded ? 'block' : 'hidden'}`}>
						{/* TODO : Link to pages */}
						<HeaderButton text="Services" href="" icon={<Book size="20"/>} style="inline-flex items-center text-black lg:text-black dark:text-OffWhite lg:dark:text-black px-3 py-2 hover:bg-Gray-2 lg:hover:bg-white dark:hover:bg-Gray-7 rounded-lg dark:hover:text-OffWhite cursor-pointer w-full lg:w-auto"/>
						<HeaderButton text="Rechercher des assos" href="" icon={<Search size="20"/>} style="inline-flex items-center text-black lg:text-black dark:text-OffWhite lg:dark:text-black px-3 py-2 hover:bg-Gray-2 lg:hover:bg-white dark:hover:bg-Gray-7 rounded-lg dark:hover:text-OffWhite cursor-pointer w-full lg:w-auto"/>
						<HeaderButton text="Partenaires" href="" icon={<Person size="20"/>} style="inline-flex items-center text-black lg:text-black dark:text-OffWhite lg:dark:text-black px-3 py-2 hover:bg-Gray-2 lg:hover:bg-white dark:hover:bg-Gray-7 rounded-lg dark:hover:text-OffWhite cursor-pointer w-full lg:w-auto"/>
					</div>
				</div>
			</nav>
		</header>
    );
}
