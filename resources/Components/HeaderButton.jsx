export default function HeaderButton({text = '', href = '', icon, style}) {
    return (
        <a className={style || "inline-flex items-center px-3 py-2 hover:bg-white dark:hover:bg-Gray-7 rounded-lg dark:hover:text-OffWhite cursor-pointer"} href={href}>
            {icon}
            <span class="ms-2">{text}</span>
        </a>
    );
}
