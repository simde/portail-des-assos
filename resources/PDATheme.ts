import type { CustomFlowbiteTheme } from "flowbite-react";

// Define custom theme rules for flowbite-react components here :
const PDATheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            initials: {
                text: "font-medium text-Gray-7 dark:text-white",
                base: "relative inline-flex items-center justify-center overflow-hidden bg-white dark:bg-Gray-7",
            },
        },
    },
    toggleSwitch: {
        toggle: {
            checked: {
                color: {
                    primary: "border-Yellow-1 bg-Yellow-1",
                },
            },
        },
    },
};

export default PDATheme;
