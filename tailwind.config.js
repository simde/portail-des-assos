import defaultTheme from 'tailwindcss/defaultTheme';
import flowbite from "flowbite-react/tailwind";

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.jsx',
        './resources/**/*.vue',
        flowbite.content(),
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
                'Text': "var(--Text)",
                'Logo': "var(--Logo)",
            },
            colors: {
                'Yellow-1': '#F9D211', // BDE accents
                'Yellow-2': '#FFE565',
                'Yellow-3': '#FFF3B6', // Cemetery
                'Yellow-4': '#FFF8D8',

                'OffWhite': '#F9FAFB', // text in dark mode

                'Gray-1': '#D7DBE0',
                'Gray-2': '#B4BBC5',
                'Gray-3': '#8B96A5',
                'Gray-4': '#6C798B',
                'Gray-5': '#576272',
                'Gray-6': '#474F5D',
                'Gray-7': '#3D444F', // blocks
                'Gray-8': '#31353E', // background modified
                'Gray-9': '#24272D', // background
                
                'GreenBack': '#ACB889', // background of access accepted
                'GreenText': '#4E6411', // text of access accepted

                'RedBack': '#B8898A', // background refused access
                'RedText': '#641112', // text refused access

                'PAE': '#EB7830',
                'PVDC': '#9A49E3',
                'PSEC': '#99C029',
                'PTE': '#249CD3',

                'StrokeInLightMode': '#E5E7EB',
            }
        },
    },
    plugins: [
        flowbite.plugin(),
    ],
};
