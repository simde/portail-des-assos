| Milestones               | Tâches                                                                       | Priorités | Durées estimées (en h) | Issues |
| ------------------------ | ---------------------------------------------------------------------------- |:---------:|:----------------------:| ------ |
| Cadrage et Planification | Rédaction du cahier des charges                                              |     1     |          10h           |        |
| Cadrage et Planification | Sondage, récupération du besoin                                              |     1     |           2h           |        |
| Cadrage et Planification | Initialisation de l'espace de travail (gitlab)                               |     1     |           1h           |        |
| Cadrage et Planification | Workflow git                                                                 |     1     |           2h           |        |
| Cadrage et Planification | Rédaction de la backlog et estimation                                        |     2     |           4h           |        |
| Conception               | Diagramme de la BDD                                                          |     3     |           5h           |[#16](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/16)[#17](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/17)|
| Conception               | User Journey                                                                 |     3     |           4h           |[#15](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/15)|
| Conception               | Création de wireframes, storyboard, maquettage                               |     4     |          30h           |[#7](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/7)|
| Conception               | Identification de la charte graphique                                        |     2     |           2h           |        |
| Montée en compétences    | Montée en compétences Laravel, Tailwind (Flowbite), Gitlab, React            |     0     |          30h           |        |
| Création du projet       | Création projet Laravel                                                      |     5     |           3h           |[#21](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/21)|
| Création du projet       | Migration des modèles de données                                             |     6     |           5h           |        |
| Création du projet       | Initialisation du plugin langues                                             |     5     |           2h           |        |
| Création du projet       | Header                                                                       |     6     |           8h           |        |
| Création du projet       | Footer                                                                       |     6     |           3h           |        |
| Authentification         | Lien avec le oauth du SIMDE                                                  |     7     |           5h           |        |
| Recherche Assos          | Formulaire de recherche des assos                                            |     6     |          15h           |        |
| Recherche Assos          | Gérer les filtres                                                            |     8     |           5h           |        |
| Consultation Assos       | Description de l'Assos                                                       |     7     |           9h           |        |
| Consultation Assos       | Page assos                                                                   |     6     |           8h           |        |
| Consultation Assos       | Consulter les membres                                                        |     9     |           7h           |        |
| Consultation Assos       | Harmoniser le front                                                          |     9     |           6h           |        |
| Gérer Assos              | Accepter/Refuser des membres                                                 |     7     |          10h           |        |
| Gérer Assos              | Système de rôle                                                              |     6     |          20h           |        |
| Gérer Assos              | Page assos en version admin                                                  |     7     |           8h           |        |
| API                      | API Logo                                                                     |    10     |           4h           |[#18](https://gitlab.utc.fr/simde/portail-des-assos/-/issues/18)|
| API                      | API liste de membre                                                          |    10     |           4h           |        |
| Annexe                   | Création du Light Mode (dynamique)                                           |    10     |           4h           |        |
| Annexe                   | Loading page+404                                                             |    11     |          404h          |        |
| Page Compte utilisateur  | Récupération des infos de l'utilisateur                                      |     6     |           5h           |        |
| Page Compte utilisateur  | Historique des associations                                                  |     8     |           6h           |        |
| Page Compte utilisateur  | Demande accès                                                                |     7     |          15h           |        |
| Page Compte utilisateur  | Page Front                                                                   |     8     |           6h           |        |
| Changement langue        | Dropdown dans le header section utilisateur (changement de langue adaptatif) |    10     |           2h           |        |
| Assos favorites          | Afficher favoris                                                             |     8     |           5h           |        |
| Assos favorites          | Ajouter favoris                                                              |     7     |           1h           |        |
| Assos favorites          | Retirer des favoris (bouton clic droit + bouton page Assos)                  |     9     |           2h           |        |
| Documentation            | Rédaction du [README](../README.md)                                          |    11     |           4h           |        |
| Page Accueil             | Page partenaire                                                              |     8     |           6h           |        |
| Page Accueil             | Page service                                                                 |     7     |           6h           |        |
| Page Accueil             | Harmoniser le front                                                          |     9     |          10h           |        |
| Marge d'erreur           | Tâches imprévues                                                             |     -     |       1 semestre       |        |
| **TOTAL**                | -                                                                            |     -     |        **300h**        |        |
