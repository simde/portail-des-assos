This file lists the custom tailwind component classes and theme extensions we have added.

# Colors

#### Defined in `tailwind.config.js`

`Yellow-1` : #F9D211 // BDE accents


`Yellow-2` : #FFE565

`Yellow-3` : #FFF3B6 // Cemetery

`Yellow-4` : #FFF8D8


`OffWhite` : #F9FAFB // text in dark mode 


`Gray-1` : #D7DBE0

`Gray-2` : #B4BBC5

`Gray-3` : #8B96A5

`Gray-4` : #6C798B

`Gray-5` : #576272

`Gray-6` : #474F5D

`Gray-7` : #3D444F // blocks

`Gray-8` : #31353E // background modified

`Gray-9` : #24272D // background


`GreenBack` : #ACB889 // background of access accepted

`GreenText` : #4E6411 // text of access accepted

`RedBack` : #B8898A // background refused access

`RedText` : #641112 // text refused access
  

`PAE` : #EB7830

`PVDC` : #9A49E3

`PSEC` : #99C029

`PTE` : #249CD3


`StrokeInLightMode` : #E5E7EB

#### example :

	<div  className='bg-Yellow-1 p-4 flex gap-4'>
		<div  className='bg-PAE rounded w-fit ml-auto'>
			<p  className='px-2 py-1'>PAE</p>
		</div>
		<div  className='bg-Gray-9 rounded w-fit mr-auto'>
			<p  className='text-OffWhite px-2 py-1'>Text</p>
		</div>
	</div>


  
# Component classes

#### Defined in `resources/css/app.css`

`PortailAssos` /* for 'Portail des Assos' in the header */


`Title`


`ParagraphTitle`

`Paragraph`

`Small` // small text


`Buttons`

`ButtonSmall`

#### example :

	<p className="PortailAssos">Portail des Assos</p>
	<Link className='ButtonSmall rounded-lg px-2 py-1'>
		Services
	</Link>
	<h1 className='Title py-4'>Bienvenue sur le portail des Assos !</h1>


# Fonts
#### Defined in `app.css` as css variables :

`--Text` : "Inter", system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;

`--Logo` : "AveriaGruessaLibre", system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;

##### `--Text` font family is applyed by default to `body`.

#### Defined in `tailwind.config.js` as tailwind theme extensions :

`Text` : "var(--Text)" // for text

`Logo` : "var(--Logo)" // for 'Portail des Assos' in the header

  #### example :

	<p  className='font-Text'>Text</p>
	<p  className='font-Logo'>Portail des Assos</p>
